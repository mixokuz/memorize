//
//  CardCell.swift
//  memorize
//
//  Created by Михаил on 08.12.2021.
//

import UIKit

class CardCell: UICollectionViewCell {
    
    var cardView: CardView!
    
    var data: String? {
        didSet {
            guard let data = data else { return }
            cardView.text = data.self
            cardView.label.text = data.self
            cardView.label.textColor = .red
        }
    }
//    private var cardColor = UIColor.yellow
    
//    func setCardColor(color: UIColor) {
//        cardColor = color
//    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        cardView = CardView(text: data ?? "")
        cardView.text = "hhhh"
        cardView.layer.cornerRadius = 10
        cardView.label.textColor = .red
//        cardView.backgroundColor = cardColor
        
        addSubview(cardView)
        
        cardView.widthAnchor.constraint(equalTo: contentView.widthAnchor).isActive = true
        cardView.heightAnchor.constraint(equalTo: contentView.heightAnchor).isActive = true
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

