//
//  CardView.swift
//  memorize
//
//  Created by Михаил on 07.12.2021.
//

import UIKit

class CardView: UIView {
    
    var imageView : UIImageView!
    var label: UILabel!
    var text: String
    
    init(text: String) {
        self.text = text
        super.init(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
        
        configureText(string: text)
        isUserInteractionEnabled = true
        configureTapGesture()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func configureTapGesture() {
        let tap = UIGestureRecognizer(target: self, action: #selector(handleTap))
        addGestureRecognizer(tap)
    }
    
    @objc func handleTap(sender: UITapGestureRecognizer) {
        print("text")
    }
    
    func configureText(string: String) {
        label = UILabel()
        label.text = string
        label.textColor = .red
        addSubview(label)
        
        label.translatesAutoresizingMaskIntoConstraints = false
        label.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 1).isActive = true
        label.heightAnchor.constraint(equalTo: heightAnchor, multiplier: 1).isActive = true
        
    }
    
    
}
