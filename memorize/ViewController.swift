//
//  ViewController.swift
//  memorize
//
//  Created by Михаил on 07.12.2021.
//

import UIKit

class ViewController: UIViewController {
    var cardsImages: [String] = ["1", "2", "3", "4", "5"]
    var collectionView: UICollectionView!
    var resetButton: UIButton!
    
    var cards: [String] = []
    var tappedCards: [Int] = []
    
    var solvedCards = [String]()
    
    var chosenCards = [String]()
    
    override func viewDidLoad() {
        cards = createListOfCards()
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        createCollectionView()
        
        createButton()
    }
    
    func createListOfCards() -> [String] {
        var starterList = [String]()
        starterList.append(contentsOf: cardsImages)
        starterList.append(contentsOf: cardsImages)
        return starterList.shuffled()
    }
    
    func createCollectionView() {
        collectionView = UICollectionView(frame: view.bounds, collectionViewLayout: UICollectionViewFlowLayout())
        collectionView.backgroundColor = .black
        view.addSubview(collectionView)
        
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(CardCell.self, forCellWithReuseIdentifier: "cell")
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.widthAnchor.constraint(equalTo: view.widthAnchor,constant: -100).isActive = true
        collectionView.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: 0.5).isActive = true
        collectionView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
    }
    
    func createButton() {
        resetButton = UIButton()
        resetButton.addTarget(self, action: #selector(resetGame), for: .touchUpInside)
        resetButton.backgroundColor = .blue
        resetButton.layer.cornerRadius = 10
        
        var label = UILabel()
        label.text = "Reset all"
        label.textColor = .white
        label.font = .boldSystemFont(ofSize: 20)
        
        resetButton.addSubview(label)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.centerXAnchor.constraint(equalTo: resetButton.centerXAnchor).isActive = true
        label.centerYAnchor.constraint(equalTo: resetButton.centerYAnchor).isActive = true
        
        view.addSubview(resetButton)
        
        resetButton.translatesAutoresizingMaskIntoConstraints = false
        resetButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        resetButton.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.3).isActive = true
        resetButton.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: 0.1).isActive = true
        resetButton.topAnchor.constraint(equalTo: collectionView.bottomAnchor, constant: 100).isActive = true
//        resetButton.topAnchor.constraint(equalTo: collectionView.bottomAnchor,+ 100).isActive = true
    }
    
    @objc func resetGame() {
        cards = cards.shuffled()
        chosenCards.removeAll()
        solvedCards.removeAll()
        
        collectionView.reloadData()
        for card in (0...cards.count) {
            collectionView.cellForItem(at: [0, card])?.backgroundColor = .red
        }
    }
    
}

extension ViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! CardCell
        cell.backgroundColor = .red
//        cell.cardView.label.textColor = .black
        cell.data = cards[indexPath.item]
        if solvedCards.contains(where: { cell.data == $0 }) {
//            cell.backgroundColor = .clear
            cell.isUserInteractionEnabled = false
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return cards.count
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("\(indexPath.row) was tapped")
        var backgroundColor = collectionView.cellForItem(at: indexPath)?.backgroundColor
        print(backgroundColor == .red)
        if backgroundColor == .red {
//            checking if we can tap it
            collectionView.cellForItem(at: indexPath)?.backgroundColor = .white
            print(backgroundColor == .red)
            tappedCards.append(indexPath.row)

            if tappedCards.count >= 2 {
                if cards[tappedCards[0]] == cards[tappedCards[1]] {
                    print("2 of a kind")
                    solvedCards.append(cards[tappedCards[0]])
                    print(solvedCards.last ?? "empty")
                    collectionView.cellForItem(at: indexPath)?.backgroundColor = .black
                    collectionView.cellForItem(at: [0, tappedCards[0]])?.backgroundColor = .black
                    tappedCards.removeAll()
                } else {
                    if tappedCards.count == 3 {
                        print("quite different")
                        collectionView.cellForItem(at: [0, tappedCards[0]])?.backgroundColor = .red
                        tappedCards[0] = indexPath.row
                        collectionView.cellForItem(at: [0, tappedCards[1]])?.backgroundColor = .red
                        tappedCards.remove(at: tappedCards.count - 1)
                        tappedCards.remove(at: tappedCards.count - 1)
                    }
                }
            } else {
                
            }
        }

    }
    
    func endOfTurn() {
        if tappedCards.count > 2 {
            if cards[tappedCards[0]] == cards[tappedCards[1]] {
                print("2 of a kind")
                solvedCards.append(cards[tappedCards[0]])
                print(solvedCards.last ?? "empty")
            } else {
                print("quite different")
                
            }
        }
        tappedCards = []
    }
    
    func endGame() {
        print("Game finished")
    }
    func startGame() {
        
    }
    
}

